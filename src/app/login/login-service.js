(function () {

    'use strict';

    angular
        .module('util.LoginServices', ['util.AuthenticationServices', 'util.ConfigServices'])
        .factory('LoginService', function (AuthenticationService, ConfigService, $http, UserService) {

            var LoginService = function(){};

            /**
             * Submit a users details for login
             * Store authentication token returned in local storage / cookie
             *
             * @param  {[type]}   formData [description]
             * @param  {Function} successCb [description]
             * @param  {[type]}   errorCb    [description]
             * @return {[type]}            [description]
             *
             *
             *******************************************************************************************
             * Currently un-used. We're using Parse to login users, see ParseService
             *******************************************************************************************
             *
             *
             */
            LoginService.prototype.login = function (formData, successCb, errorCb) {
                var detailsForAuth = jQuery.base64.encode(formData.Email + ':' + formData.Password);
                var userDetails = {
                    email: formData.Email
                };

                $http({
                    method: 'POST',
                    url: ConfigService.webServiceUrl() + '/login'
                    // ,
                    // headers: {
                    //     'Authorization': 'Basic ' + detailsForAuth
                    // }
                }).
                success(function(data, status, headers, config) {
                    AuthenticationService.prototype.setUserSession(data.Data);
                    UserService.prototype.setUser(userDetails);
                    successCb (data, status, headers, config);
                }).
                error(function(data, status, headers, config) {
                    error (data, status, headers, config);
                });
            };

            return LoginService;
        });
}());