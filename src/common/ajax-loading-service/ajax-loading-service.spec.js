describe('util.AjaxLoadingServices', function() {

    var AjaxLoadingService;
    AjaxLoadingService = null;

    beforeEach(module('util.AjaxLoadingServices'));

    beforeEach(inject(function(_AjaxLoadingService_) {
        AjaxLoadingService = _AjaxLoadingService_;
        return AjaxLoadingService;
    }));

    it( 'should pass a dummy test', inject( function() {
        expect( AjaxLoadingService ).toBeTruthy();
    }));
});
