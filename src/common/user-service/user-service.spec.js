describe('util.UserServices', function() {

    var UserService;
    UserService = null;

    beforeEach(module('util.UserServices'));

    beforeEach(inject(function(_UserService_) {
        UserService = _UserService_;       
        return UserService;
    }));

    it( 'should pass a dummy test', inject( function() {
        expect( UserService ).toBeTruthy();
    }));
});
