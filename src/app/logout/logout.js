(function() {

    'use strict';

    angular.module( 'helloJC.logoutApp', [
        'ui.router'
        ])

    .config(function config( $stateProvider ) {
        $stateProvider
            .state( 'logout', {
                url: '/logout',
                views: {
                    'main': {
                        controller: 'LogoutCtrl'
                        // No template, we take the user straight to login state
                        // templateUrl: 'logout/logout.tpl.html'
                    }
                },
                data: {
                    authReq: false,
                    pageTitle: 'Logout'
                }
            });
    })

    .controller( 'LogoutCtrl', function LogoutCtrl ($http, $scope, $state) {
        /**
         * Initialise app
         */
        var init = function() {
            delete $http.defaults.headers.common['Authorization'];
            Parse.User.logOut();
            $state.transitionTo('login');
        };

        init();
    });

}());