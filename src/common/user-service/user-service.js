(function () {

    'use strict';

    angular
        .module('util.UserServices', ['util.ConfigServices', 'ngStorage'])
        .factory('UserService', function ($http, $sessionStorage, ConfigService) {

            var UserService = function(){};

            /**
             * Set a users details in local storage / cookie
             * @param {object} params Additional params to add
             */
            UserService.prototype.setUser = function(params) {
                // Extend the details if something already exists
                var user = UserService.prototype.getUser();
                if (user) {
                    angular.extend(user, params);
                    $sessionStorage.user = user;
                } else {
                    $sessionStorage.user = params;
                }
            };


            /**
             * Get a users details from local storage / cookie / 3d party service
             * @return {object/boolean} Return the user or false if none
             */
            UserService.prototype.getUser = function() {
                // var user = $sessionStorage.user;
                var user = Parse.User.current();

                if (user !== null) {
                    user = Parse.User.current().attributes;
                }
                return (user !== null && user !== undefined && Parse.User.current().attributes !== undefined) ? user : false;
            };

            return UserService;
        });
}());