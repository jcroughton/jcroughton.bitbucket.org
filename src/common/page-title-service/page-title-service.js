(function () {

    'use strict';

    angular
        .module('util.PageTitleServices', [])
        .factory('PageTitleService', function($document) {
            var suffix, title;

            suffix = title = "";

            return {
                setPageSuffix: function(s) {
                    return suffix = s;
                },

                getPageSuffix: function() {
                    return suffix;
                },

                setPageTitle: function(t) {
                    var titleSupplied = t;

                    // JC Update
                    // If we're calculating a page title (i.e. not just a string)
                    if(typeof(t) == 'function') {
                        titleSupplied = t();
                    }

                    if (suffix !== "") {
                        title = titleSupplied + suffix;
                    } else {
                        title = titleSupplied;
                    }

                    return $document.prop('title', title);
                },

                getPageTitle: function() {
                    return $document.prop('title');
                }
            };
    });

}());