(function () {

    'use strict';

    // Instagram service

    angular
        .module('util.TwitterServices', ['util.ConfigServices'])
        .factory('TwitterService', ['ConfigService', '$filter', '$http', '$q', function(ConfigService, $filter, $http, $q) {

            var TwitterService = function(){};

            /**
             * Get any updates since the lastId provided
             * @param  {[type]} lastId [description]
             * @return {[type]}        [description]
             */
            TwitterService.prototype.getUpdates = function (lastId) {
                // var deferred = $q.defer();

                // var paramsObj = {
                //     callback: 'JSON_CALLBACK',
                //     access_token: ConfigService.instagramAccessToken,
                //     min_id: (lastId ? lastId : '') // If no lastId get back all that we can for this endpoint (instagram returns 20 before pagination)
                // };

                // $http({
                //     method: 'JSONP',
                //     url: ConfigService.instagramApiUrl() + '/users/' + ConfigService.instagramId + '/media/recent',
                //     params: paramsObj

                // }).
                // success(function(data, status, headers, config) {
                //     // API returns updates INCLUDING the min_id item
                //     // Strip that off
                //     var filteredData = $filter('filter')(data.data, function(elem){
                //         return elem.id !== lastId;
                //     });

                //     deferred.resolve(filteredData, status, headers, config);
                // }).
                // error(function(data, status, headers, config) {
                //     deferred.reject(data, status, headers, config);
                // });

                // return deferred.promise;







                // We call the Parse cloud code for this (as we want to store
                // the tokens etc there for security)

                // Use since_id param on Twitter API to get since given id









            };


            return TwitterService;
        }]);
}());