(function () {

    'use strict';

    angular
        .module('util.AuthenticationServices', ['ngStorage', 'util.UserServices'])
        .factory('AuthenticationService', ['$http', '$sessionStorage', 'UserService', function ($http, $sessionStorage, UserService) {

            var AuthenticationService = function(){};

            /**
             * Return true/false of whether a user is authenticated based on local
             * localStorageService / cookie / value returned by 3rd party service
             * Or return the actual session token
             * @param  {boolean}            type     - Used to decide what we'll return
             * @param  {Function}           callback - Function to callback
             * @return {boolean / string}            - Return appropriate value
             */
            AuthenticationService.prototype.getUserSession = function(type, callback) {
                if (type === true) {
                    // return !!($sessionStorage.userSession);
                    return !!(UserService.prototype.getUser());
                } else {
                    // return 'Session ' +  $sessionStorage.userSession;
                    return UserService.prototype.getUser();
                }
            };


            /**
             * Set the users authentication token in local localStorageService / cookie
             * @param {string}  sessionToken - Token to set in local storage / cookie
             * @param {boolean} callback     - Set variable result
             */
            AuthenticationService.prototype.setUserSession = function(sessionToken, callback) {
                return !!($sessionStorage.userSession = sessionToken);
            };


            /**
             * Delete the users authentication token from local localStorageService / cookie
             * @param  {Function} callback - Function to callback
             * @return {boolean}           - Delete variable result
             */
            AuthenticationService.prototype.deleteUserAuthentication = function(callback) {
                return !! (delete $sessionStorage.userSession);
            };


            /**
             * Receive whether the stateRequiresAuth
             * Return the combination of that and whether the user currently has a session
             * OR
             * Just return whether the user is currently authenticated (i.e. called without stateRequiresAuth)
             *
             * @param  {boolean}  stateRequiresAuth - Does the state require authentication
             * @param  {Function} callback          - Function to callback
             * @return {Boolean}                    - Is the user authenticated
             */
            AuthenticationService.prototype.isAuthenticated = function(stateRequiresAuth, callback) {
                if (stateRequiresAuth !== undefined) {
                    return (stateRequiresAuth && !AuthenticationService.prototype.getUserSession(true)) ? false : true;
                } else {
                    return AuthenticationService.prototype.getUserSession(true);
                }
            };


            return AuthenticationService;
    }]);

}());