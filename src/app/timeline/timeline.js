(function() {

    'use strict';

    /**
    * Each section of the site has its own module. It probably also has
    * submodules, though this boilerplate is too simple to demonstrate it. Within
    * `src/app/home`, however, could exist several additional folders representing
    * additional modules that would then be listed as dependencies of this one.
    * For example, a `note` section could have the submodules `note.create`,
    * `note.delete`, `note.edit`, etc.
    *
    * Regardless, so long as dependencies are managed correctly, the build process
    * will automatically take take of the rest.
    *
    * The dependencies block here is also where component dependencies should be
    * specified, as shown below.
    */
    angular.module( 'helloJC.timelineApp', [
        'ui.router'
        ])

    /**
    * Each section or module of the site can also have its own routes. AngularJS
    * will handle ensuring they are all available at run-time, but splitting it
    * this way makes each module more "self-contained".
    */
    .config(function config( $stateProvider ) {
        $stateProvider
            .state('timeline', {
                url: '/timeline',
                views: {
                    'main': {
                        templateUrl: 'timeline/timeline.tpl.html',
                        controller: 'TimelineCtrl'
                    }
                },
                data: {
                    authReq: false,
                    pageTitle: 'Timeline'
                }
        });
    })

    /**
    * And of course we define a controller for our route.
    */
    .controller( 'TimelineCtrl', function TimelineCtrl( ParseService, $scope, TimelineService ) {

        var init = function(ParseService, $scope) {
            ////////////////////////////////////////////////////////////////////
            // TODO
            // Provide the data for the timeline to this conrtoller via a resolve?
            // Would ensure timeline not laoded until ready
            ////////////////////////////////////////////////////////////////////

            // Get timeline data from parse and attach it to a timeline
            ParseService.prototype.getTimelineItemData().then(function() {
                // Success
                TimelineService.prototype.attachTimeline();

            }, function(data) {
                // Error
            });
        };

        init(ParseService, $scope);
    });
})();