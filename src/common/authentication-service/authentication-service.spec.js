xdescribe('util.AuthenticationServices', function() {

    var AuthenticationService;
    AuthenticationService = null;

    beforeEach(module('util.AuthenticationServices'));

    beforeEach(inject(function(_AuthenticationService_) {
        // return AuthenticationService = _AuthenticationService_;

        AuthenticationService = _AuthenticationService_;
        AuthenticationService.prototype.deleteUserAuthentication();
        return AuthenticationService;

    }));

    it('should set (and get) user authentication', inject(function() {

        var token = {
          access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzZXNzaW9uIGlzc3VlciIsImF1ZCI6Imh0dHA6Ly9zZXNzaW9uLnR0IiwibmJmIjoxMzc2NTY5NDk2LCJleHAiOjEzNzY2MDU0OTYsInVuaXF1ZV9uYW1lIjoibGFsYWxhIn0.Bn40l_c9svVdQ0gEo-8ePbkSTlD9iH3cWltyJkBtfwI",
          expires_in: 36000.0
        };

        AuthenticationService.prototype.setUserSession(token.access_token);
        return expect( AuthenticationService.prototype.getUserSession() ).toBeTruthy();
    }));

    // state doesn't need auth, User not logged in
    it('should allow a non-logged-in user to see states (routes) that don\'t need authorisation', inject(function() {
        var stateRequiresAuth = false;
        return expect( AuthenticationService.prototype.isAuthenticated(stateRequiresAuth) ).toBeTruthy();
    }));

    // state DOES need auth, User not logged in
    it('should NOT allow a non-logged-in user to see states (routes) that DO need authorisation', inject(function() {
        var stateRequiresAuth = true;
        return expect( AuthenticationService.prototype.isAuthenticated(stateRequiresAuth) ).toBeFalsy();
    }));

    // User IS logged in
    it('should allow a logged-in user to see all states (routes)', inject(function() {
        var stateRequiresAuth = true;
        var token = {
          access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzZXNzaW9uIGlzc3VlciIsImF1ZCI6Imh0dHA6Ly9zZXNzaW9uLnR0IiwibmJmIjoxMzc2NTY5NDk2LCJleHAiOjEzNzY2MDU0OTYsInVuaXF1ZV9uYW1lIjoibGFsYWxhIn0.Bn40l_c9svVdQ0gEo-8ePbkSTlD9iH3cWltyJkBtfwI",
          expires_in: 36000.0
        };

        AuthenticationService.prototype.setUserSession(token.access_token);
        return expect( AuthenticationService.prototype.isAuthenticated(stateRequiresAuth) ).toBeTruthy();
    }));

});
