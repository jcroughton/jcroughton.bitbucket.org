(function () {

    'use strict';

    angular
        .module('util.FormServices', ['util.UtilityServices', 'util.GlobalErrorServices'])
        .factory('FormService', ['UtilityService', 'GlobalErrorService', function (UtilityService, GlobalErrorService) {

            var FormService = function(){};

            // Helper function to handle errors returned by the API
            // Receive the error obj from API and extend the forms error object with it
            // This is then displayed in the validation summary

            // apiError returns in _formName_status_ format as it's subsequently run
            // through the localisation resources filter
            // Unexpected errors can be output using the unexpectedApiError property,
            // i.e.
            // <li ng-show="bookmarkFrm.$error.unexpectedApiError">
            //      {{ bookmarkFrm.$error.unexpectedApiError }}
            // </li>
            FormService.prototype.handleApiError = function(status, form) {
                var formIdentifier = !!(form.$name) ? form.$name : form.$id;

                // Detect if we're using using i18n / resource files
                // and thus whether to return the expectedError
                var expectedError = null;
                if (typeof localize !== 'undefined') {
                    expectedError = '_' + formIdentifier + '_' + status + '_';
                }

                var unexpectedError = null;
                if (status) {
                    unexpectedError = status.code + ' - ' + status.message;
                }

                angular.extend(form.$error, {
                    apiError: expectedError,
                    unexpectedApiError: unexpectedError
                });
            };


            // Receive a form and the scope
            // Set the form invalid and dirty, show the validation summary
            // Re-factored to service helper function as used on several forms
            FormService.prototype.setFormDirtyShowValidation = function (form, scope) {
                form.$invalid = true;
                // Set the form to dirty, it's been interacted with
                // Means the validators will run even if the user hasn't
                // entered anything yet
                form.$dirty = true;
                // Show the validation summary now the forms been
                // interacted with
                scope.showValidationSummary = true;
                FormService.prototype.reEnableSubmit(form);
                // Force the scope to update otherwise sometimes
                // the value is right on scope but not reflected in the template.
                scope.$apply();
            };


            // Re-enable the submit button of a form
            // Called from setFormDirtyShowValidation but using a method so we could call manually
            // i.e. during a potential error callback from the API
            FormService.prototype.reEnableSubmit = function(form) {
                jQuery('[name="' + form.$name + '"]').find(':submit').prop('disabled', false);
            };


            return FormService;
    }]);

}());