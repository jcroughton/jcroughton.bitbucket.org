(function () {

    'use strict';

    angular
        .module('util.UtilityServices', ['ngStorage', 'util.PageTitleServices', 'util.UserServices'])
        .factory('UtilityService', ['PageTitleService', 'ParseService', '$rootScope', '$sessionStorage', 'UserService', function(PageTitleService, ParseService, $rootScope, $sessionStorage, UserService) {

            var UtilityService = function(){};

            /**
             * Setup the app
             * @return {[type]} [description]
             */
            UtilityService.prototype.appSetup = function() {
                $(document.body).removeClass('no-js').addClass('js');
                Parse.initialize('1dP7B409kbocnrxBfn5SKImJehtpKVeyTj81WQFs', 'YTxPfdw5LIcbLIvoHv21QYA6IyVF4bUSX3yMbmzf');
            };


            /**
             * Setup the given page
             * @param  {[type]} event      [description]
             * @param  {[type]} toState    [description]
             * @param  {[type]} toParams   [description]
             * @param  {[type]} fromState  [description]
             * @param  {[type]} fromParams [description]
             * @return {[type]}            [description]
             */
            UtilityService.prototype.pageSetup = function(event, toState, toParams, fromState, fromParams) {
                $rootScope.loggedInAs = UserService.prototype.getUser().username + ' ( ' + UserService.prototype.getUser().email + ')';

                // Set the suffix for the page title
                PageTitleService.setPageSuffix( 'helloJC' );

                // Only do this if we have a data property (some states may not define this, i.e. abstract state)
                if (!!toState.data && toState.data.pageTitle) {
                    PageTitleService.setPageTitle( toState.data.pageTitle  + ' : ');
                    $rootScope.currentSiteSection = UtilityService.prototype.removeSpecialCharacters(toState.data.pageTitle);
                }
            };


            /**
             * Remove special characters and spaces from the supplied string
             * @param  {string} input   String to remove special characters from
             * @return {string}         Sanitized string
             */
            UtilityService.prototype.removeSpecialCharacters = function(input) {
                if (input) {
                    return input.replace(/[^\w]/gi, '').toLowerCase();
                }
            };


            /**
             * Store a prevented state
             * User has tried to access a state that requires authentication
             * Store it so we can take them there after they login
             */
            UtilityService.prototype.setPreventedState = function (requestedState) {
                var infoToStore = null;
                if (requestedState) {
                    infoToStore = {
                        toState: requestedState.toState,
                        toParams: requestedState.toParams,
                        fromState: requestedState.fromState,
                        fromParams: requestedState.fromParams
                    };
                }
                $sessionStorage.preventedState = infoToStore;
            };


            /**
             * Return a prevented state from local localStorageService
             * @return {object/boolean}     Return either the prevented state or false (no prevented state)
             */
            UtilityService.prototype.getPreventedState = function () {
                var preventedState = $sessionStorage.preventedState;
                return (preventedState !== null && preventedState !== undefined) ? preventedState : false;
            };


            return UtilityService;
    }]);

}());