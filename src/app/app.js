(function() {

    'use strict';

    // App Module
    // Declare app level module which depends on filters, services, directives
    angular
        .module('helloJC', [
            'templates-app',
            'templates-common',
            'ui.router.state',
            'ui.router',
            'angularMoment',
            'angular-growl',
            'ngAnimate',
            'ngStorage',
            'formly',

            // 'helloJCTest', // Only when we want the mocked backend to run

            // Services
            'util.AjaxLoadingServices',
            'util.AuthenticationServices',
            'util.ConfigServices',
            'util.FormServices',
            'util.GlobalErrorServices',
            'util.LoginServices',
            'util.PageTitleServices',
            'util.ParseServices',
            'util.TimelineServices',
            'util.TwitterServices',
            'util.UserServices',
            'util.UtilityServices',

            // Directives
            'util.FormDirectives',

            // Apps
            'helloJC.daveApp',
            'helloJC.loginApp',
            'helloJC.logoutApp',
            'helloJC.timelineApp'

        ])

        .config( function myAppConfig ( formlyConfigProvider, growlProvider, $httpProvider, $locationProvider, $stateProvider, $urlRouterProvider ) {
            $locationProvider.html5Mode(false).hashPrefix('!');
            $urlRouterProvider.otherwise('/timeline');

            // Provide updated defaults for the growl plugin
            // See https://github.com/JanStevens/angular-growl-2 re server messages
            growlProvider.onlyUniqueMessages(true);
            growlProvider.globalPosition('top-left');
            $httpProvider.responseInterceptors.push(growlProvider.serverMessagesInterceptor);

            // Configure a date field template for formly
            formlyConfigProvider.setTemplateUrl({
                date: 'formly-field-date.html'
            });
        })

        .run ( function run ( AuthenticationService, $rootScope, $state, UtilityService ) {
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                // Creating a prevented state
                // Done in $stateChangeStart so controllers etc. won't fire their
                // API calls and cause errors if the user isn't logged in

                // IF we've got data THEN
                // IF the state we want needs authentication AND the user isn't authenticated THEN
                // Create a prevented state
                if (toState.data) {
                    if ((toState.data.authReq) && (!AuthenticationService.prototype.isAuthenticated())) {
                        var requestedState = {
                            event: event,
                            toState: toState,
                            toParams: toParams,
                            fromState: fromState,
                            fromParams: fromParams
                        };

                        // Store the prevented state
                        UtilityService.prototype.setPreventedState(requestedState);
                        // Needed to stop the state manager transitioning to the requested state
                        event.preventDefault();
                        // Fire a $stateChangeSuccess manually so we can complete the transition
                        $rootScope.$broadcast('$stateChangeSuccess', requestedState.event, requestedState.toState, requestedState.toParams, requestedState.fromState, requestedState.fromParams);
                    }
                }


            });

            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                // IF we have a prevented state THEN
                // Deal with it
                // ELSE
                // Clear any prevented state
                if (UtilityService.prototype.getPreventedState()) {
                    // We have a prevented state
                    // BUT, does the state just requested need auth?
                    // IF not THEN do nothing, just carry on
                    // ELSE
                    // Get the prevented state
                    // IF bookmark THEN send to login
                    // ELSE send to home login

                    if (toState.data) {
                        if (!toState.data.authReq) {
                            // do nothing, we just want the state to carry on and load
                            // Bit funky having an empty if but it's needed to make this work
                            // (in my head)
                        }
                    } else {
                        var previouslyPrevented = UtilityService.prototype.getPreventedState();
                        // if (previouslyPrevented.toState.name.indexOf('bookmark') > -1) {
                        if (!!previouslyPrevented) {
                            $state.transitionTo('timeline', previouslyPrevented.toParams, true);
                        }
                    }
                } else {
                   UtilityService.prototype.setPreventedState();
                }

                // Check if user logged in. Don't need to transition to
                // login/register if they are
                if (AuthenticationService.prototype.isAuthenticated(true)) {
                    //changed to check for full state name instead of indexOf
                    if (toState.name ==='login') {
                        $state.transitionTo('dave');
                    }
                }

                // Setup the page
                UtilityService.prototype.pageSetup(event, toState, toParams, fromState, fromParams);
            });

            // App setup
            UtilityService.prototype.appSetup();
        })

        .controller( 'AppCtrl', function AppCtrl (AuthenticationService, $rootScope, $scope, $state) {
            /**
             * Global method
             * Assertain whether the current user is authenticated
             * @return {Boolean} Is the user authenticated?
             */
            $rootScope.isLoggedIn = function () {
                return AuthenticationService.prototype.isAuthenticated(true);
            };

        });

}());


angular
    .module('helloJCTest', ['ngMockE2E', 'ngResource', 'util.ConfigServices'])
    .run(function($httpBackend, $http, $resource, ConfigService) {
        console.log('RUNNING MOCKED $httpBackend');

            // Instagram
            $httpBackend.whenGET(ConfigService.instagramApiUrl() + 'users/206213/media/recent?access_token=' + ConfigService.instagramAccessToken).respond(function(method, url) {
                console.log('MOCK /users/' + ConfigService.instagramId + '/media/recent:', method, url);
                // data, status, headers, config
                return [200, {}, {}];

                // var response = {
                //     Responses: {
                //         Response: {
                //             responseCode: "INVALID_URL",
                //             responseMessage: "Entered URL is not supported"
                //         }
                //     }
                // }

                // return [400, response, {}];
            });

        $httpBackend.whenGET(/.*/).passThrough();
        $httpBackend.whenPOST(/.*/).passThrough();
});