(function () {

    'use strict';

    angular
        .module('util.FormDirectives', [])

        /**
         * Attach some functionality to a form submission via a directive
         * http://code.realcrowd.com/on-the-bleeding-edge-advanced-angularjs-form-validation/
         * @param  {object} $rootScope - AngularJS object
         * @param  {object} $timeout   - AngularJS object
         */
        .directive('formSubmit', function($rootScope, $timeout) {
            return {
                restrict: 'A',
                require: 'form',
                link: function (scope, element, attrs, controller) {
                    element.bind('submit', function (event) {
                        // Form is valid,
                        // Disable the submit button so the user can't click it again
                        // Add a class to the form element
                        if (controller.$valid) {
                            jQuery(element).addClass('ng-submitted').find(':submit').prop('disabled', 'disabled');
                            scope.$apply();
                        }
                    });
                }
            };
        });
}());