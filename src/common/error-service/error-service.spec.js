describe('util.GlobalErrorServices', function() {

    var GlobalErrorService;
    GlobalErrorService = null;

    beforeEach(module('util.GlobalErrorServices'));

    beforeEach(inject(function(_GlobalErrorService_) {
        GlobalErrorService = _GlobalErrorService_;       
        return GlobalErrorService;
    }));

    it( 'should pass a dummy test', inject( function() {
        expect( GlobalErrorService ).toBeTruthy();
    }));
});
