(function() {

    'use strict';

    angular.module( 'helloJC.daveApp', [
        'ui.router'
        ])

    .config(function config( $stateProvider ) {
        $stateProvider
            .state('dave', {
                url: '/dave',
                views: {
                    'main': {
                        templateUrl: 'dave/dave.tpl.html',
                        controller: 'DaveCtrl'
                    }
                },
                data: {
                    authReq: true,
                    pageTitle: 'Admin'
                }
            })

            .state('dave-success', {
                url: '/dave/success',
                views: {
                    'main': {
                        templateUrl: 'dave/dave-success.tpl.html',
                        controller: 'DaveSuccessCtrl'
                    }
                },
                data: {
                    authReq: true,
                    pageTitle: 'Admin'
                }
            });
    })

    .controller( 'DaveCtrl', function DaveCtrl(ConfigService, FormService, ParseService, $scope, $state) {
        /**
         * Provide JSON data / set defaults for each field on the form
         */
        var buildForm = function() {
            $scope.formFields = [
                {
                    key: 'itemId',
                    type: 'hidden',
                },
                {
                    //the key to be used in the result values {... "caption": "johndoe" ... }
                    key: 'caption',
                    type: 'text',
                    label: 'Caption',
                    placeholder: 'Text under your screenshot',
                    required: true,
                    disabled: false //default: false
                    // description: 'Caption descriptive text'
                },
                {
                    key: 'credit',
                    type: 'text',
                    label: 'Credit',
                    placeholder: 'Credit for screenshot',
                    required: true
                },
                {
                    key: 'media',
                    type: 'text',
                    label: 'Screenshot',
                    placeholder: 'URL for screenshot',
                    required: true
                },
                {
                    key: 'thumbnail',
                    type: 'text',
                    label: 'Thumbnail',
                    placeholder: 'URL for timeline thumbnail',
                    required: true
                },
                {
                    key: 'startDate',
                    type: 'date',
                    label: 'Start Date',
                    placeholder: 'Start date of timeline entry',
                    required: true
                },
                {
                    key: 'endDate',
                    type: 'date',
                    label: 'End Date',
                    placeholder: 'End date of timeline entry (usually same as start date)',
                    required: true
                },
                {
                    key: 'headline',
                    type: 'text',
                    label: 'Headline',
                    placeholder: 'Headline for your text',
                    required: true
                },
                {
                    key: 'text',
                    type: 'textarea',
                    label: 'Text',
                    lines: '4',
                    placeholder: 'Simple text of entry',
                    required: true
                },
                {
                    key: 'tags',
                    type: 'text',
                    label: 'Tags',
                    placeholder: 'Comma delimited list of tags relating to entry'
                }
            ];
        };


        /**
         * Set options / assign methods used by the form
         */
        var initialiseForm = function() {
            $scope.showValidationSummary = false;
            $scope.formData = {};

            if (ConfigService.debug) {
                $scope.formData = {
                    // asset: {
                      caption: 'Test caption',
                      credit: 'Test credit',
                      media: 'assets/screenshots/myprojectbaby.com.jpg',
                      thumbnail: 'assets/screenshots/myprojectbaby.com32x32.gif',
                    // },
                    endDate: moment(new Date()).format('YYYY-MM-DD'),
                    headline: 'Test headline',
                    startDate: moment(new Date()).format('YYYY-MM-DD'),
                    tags: 'tag1, tag2, tag3',
                    text: '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'
                };
            }
            $scope.formOptions = {};

            /**
             * Submit the form data
             * Transition to appropriate state if successful / show validation summary if error
             * @return {[type]}
             */
            $scope.onSubmit = function() {
                $scope.master = $scope.formData;

                // Prepare the TimelineItem object.
                var itemToSave = ParseService.prototype.buildTimelineItem($scope.formData);

                var NewItem = Parse.Object.extend('TimelineItem');
                var newItem = new NewItem();

                // When logged in as admin this will work (as that Parse user
                // has correct permissions for insertions etc.)
                newItem.save(itemToSave, {
                    success: function(data) {
                        $state.transitionTo('dave-success');
                    },
                    error: function(data, error) {
                        // error
                        // Handle any API errors that we want to show on the validation summary
                        // Note, we only really expect one error from the API
                        FormService.prototype.handleApiError(error, $scope.frmCreate);
                        FormService.prototype.setFormDirtyShowValidation($scope.frmCreate, $scope);
                    }
                });
            };
        };

        /**
         * Initialise app
         */
        var init = function() {
            initialiseForm();
            buildForm();
        };

        init();
    })

    .controller( 'DaveSuccessCtrl', function DaveSuccessCtrl($scope) {
        /**
         * Initialise app
         */
        var init = function() {
        };

        init();
    });

})();