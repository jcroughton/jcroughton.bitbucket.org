describe('util.PageTitleServices', function() {
    
    var $document, PageTitleService;
    $document = null;
    PageTitleService = null;


    beforeEach(module('util.PageTitleServices'));

    beforeEach(inject(function(_$document_, _PageTitleService_) {
        $document = _$document_;
        return PageTitleService = _PageTitleService_;
    }));

    it('should set a title without a suffix', inject(function() {
        var title;
        title = "new title";
        PageTitleService.setPageTitle(title);
        return expect(PageTitleService.getPageTitle()).toEqual(title);
    }));

    it('should allow specification of a suffix', inject(function() {
        var suffix;
        suffix = " :: new suffix";
        PageTitleService.setPageSuffix(suffix);
        return expect(PageTitleService.getPageSuffix()).toEqual(suffix);
    }));

    return it('should set the title, including the suffix', inject(function() {
        var suffix, title;
        title = "New Title";
        suffix = " :: new suffix";
        PageTitleService.setPageSuffix(suffix);
        PageTitleService.setPageTitle(title);
        return expect(PageTitleService.getPageTitle()).toEqual(title + suffix);
    }));
});

