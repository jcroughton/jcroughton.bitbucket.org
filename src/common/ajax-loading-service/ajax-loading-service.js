(function () {

    'use strict';

    angular
        .module('util.AjaxLoadingServices', [])
        .factory('AjaxLoadingService', ['$q','$timeout', function ($q, $timeout) {

            var AjaxLoadingService = function(){};

            var loader = '<div class="ajax-loader"><i class="icon-spin icon-centered">Loading...</i></div>';

            // This is the default loader, it gets appended to the body tag
            // If you need another loader use immediateLoadingScreen from
            // your controller passing a DOM id i.e.
            // var loadingScreen = AjaxLoadingService.prototype.immediateLoadingScreen('#timeline');
            var loadingScreen = jQuery(loader).appendTo(jQuery('body'));
            var loadingTimeout = null;


            /**
             * Hide the loading screen
             * Cancel the timeout, decrement the numLoadings and fadeout + remove
             * the element from the page
             * @return {[type]} [description]
             */
            AjaxLoadingService.prototype.hideLoadingScreen = function () {
                // Cancel the timeout
                $timeout.cancel(loadingTimeout);
                loadingScreen.fadeOut(750, function(){
                    loadingScreen.remove();
                });
            };


            /**
             * Show a loading overlay
             * Delay it so if the API responds quickly it'll be cancelled and not shown
             * (to avoid flicker)
             * @return {[type]} [description]
             */
            AjaxLoadingService.prototype.showLoadingScreen = function () {
                loadingTimeout = $timeout(function() {
                    loadingScreen.show();
                }, 300);
            };


            /**
             * Helper method to add a loading overlay to the screen immediately
             * Calling method responsible for removing!
             * @param  {[type]} containerToAppendTo Container ID to append the laoder to otherwise it'll append to the BODY tag
             * @return {[type]}                     [description]
             */
            AjaxLoadingService.prototype.immediateLoadingScreen = function(containerToAppendTo) {
                // Cancel any potential loading timeout fired by http request
                $timeout.cancel(loadingTimeout);
                loadingTimeout = null;

                // Where should the loader be appended?
                if (!containerToAppendTo) {
                    containerToAppendTo = 'body';
                }

                // return the loader. Append an 'immediate' class so we know this was the one placed on the page
                return jQuery(loader).addClass('immediate').appendTo(jQuery(containerToAppendTo)).show();
            };


            return AjaxLoadingService;
        }])
        .config(function ($httpProvider) {
            $httpProvider.responseInterceptors.push(function($q, AjaxLoadingService) {

                return function (promise) {
                    AjaxLoadingService.prototype.showLoadingScreen();

                    return promise.then(function(successResponse) {
                         // success
                        AjaxLoadingService.prototype.hideLoadingScreen();
                        return successResponse;
                    }, function(errorResponse) {
                        // error
                        // deduct from numLoadings
                        // check to see if we should hide the loading screen
                        AjaxLoadingService.prototype.hideLoadingScreen();
                        return $q.reject(errorResponse);
                    });
                };
            });
    });
}());