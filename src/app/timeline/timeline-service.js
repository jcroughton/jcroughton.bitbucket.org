(function () {

    'use strict';

    angular
        .module('util.TimelineServices', ['util.ConfigServices'])
        .factory('TimelineService', ['ConfigService', function(ConfigService) {

            var TimelineService = function(){};

            /**
             * Attach a timeline to the given embed_id
             * The JS for the timeline is loaded from the CDN
             * @return {[type]} [description]
             */
            TimelineService.prototype.attachTimeline = function() {
                var timelineObj = {
                    // debug:       true,
                    embed_id:           'story-timeline',
                    hash_bookmark:      false,
                    height:             ConfigService.timelineHeight,
                    source:             ConfigService.timelineSource,
                    start_at_end:       true,
                    start_zoom_adjust:  ConfigService.timelineInitialZoom,
                    type:               'timeline',
                    width:              ConfigService.timelineWidth
                };

                createStoryJS(timelineObj);
            };


            return TimelineService;
    }]);

}());