(function () {

    'use strict';

    // global error manager
    // http://blog.tomaka17.com/2012/12/random-tricks-when-using-angularjs/
    // Updated by JC - Add a modal type background to the page underneath the alert messages

    angular.module( 'util.GlobalErrorServices', [
        'util.ConfigServices'
        ])

    .factory('GlobalErrorService', ['ConfigService', function (ConfigService) {

        var GlobalErrorService = function(){};

        GlobalErrorService.prototype.raiseError = function(content, cl) {
            GlobalErrorService.prototype.showMessage(content, cl, ConfigService.errorTimeout);
        };


        GlobalErrorService.prototype.showMessage = function(content, cl, time) {
            var messagesContainerCSS = {
                left: '50%',
                position: 'fixed',
                marginLeft: '-35%',
                top: '20%',
                width: '70%',
                zIndex: '999999'
            };

            var modalBackdropCSS = {
                left: '50%',
                position: 'fixed',
                marginLeft: '-35%',
                top: '20%',
                width: '70%',
                zIndex: '999999 !important'
            };

            var messageContainer = jQuery('.messagesContainer');
            if (!messageContainer.length) {
                messageContainer = jQuery('<div/>')
                    .css(messagesContainerCSS)
                    .addClass('messagesContainer')
                    .appendTo('body');
            }

            var btnClose = "<button class='close' href='#'>&times;</button>";

            // Only add backdrop if we don't have one
            if (!jQuery('.messagesContainer .modal-backdrop').length) {
                jQuery('<div/>')
                    .addClass('modal-backdrop')
                    .appendTo(messageContainer);
            }

            jQuery('<div/>')
                .addClass('message')
                .addClass(cl)
                .hide()
                .fadeIn('fast')
                .delay(time)
                .fadeOut('fast', function() {
                    jQuery(this).siblings('.modal-backdrop').remove();
                    jQuery(this).remove();
                })
                .appendTo(messageContainer)
                .html(btnClose + content);

            // Click handler for closing messages and removing the modal backdrop once the last message
            // is closed
            $('.close').on('click', function () {
                jQuery(this).parent('.message').fadeOut('fast').remove();
                if (jQuery('.messagesContainer').find('.message').length === 0) {
                    jQuery('.modal-backdrop').fadeOut('fast').remove();
                }
            });
        };


        // Receive inputs and display an error / warning / info message (bootstrap style overlay)
        GlobalErrorService.prototype.messageToDisplay = function(responseStatus, responseMessage, style) {
            var message = '<p><strong>Ooops, something went wrong.</strong></p>' +
                '<p><strong>Status</strong><br>' + responseStatus + '</p>' +
                '<p><strong>Message</strong><br>' + responseMessage + '</p>' +
                '</div>';
                GlobalErrorService.prototype.showMessage(message, style, ConfigService.errorTimeout);
        };


        // Recieve a json object and return the key and value as a string
        GlobalErrorService.prototype.getKeyValue = function (obj) {
            var result;
            $.each(obj, function(k, v) {
                result =  k + ': ' + v;
            });
            return result;
        };


        // Get an object from a group of json objects where a specific attribute has the given id
        // http://stackoverflow.com/questions/4992383/use-jquerys-find-on-json-object
        // Duplicate of what's in UtilityService as couln't seem to include UtilityService without circular dependancy error
        GlobalErrorService.prototype.getObjects = function(obj, key, val) {
            var objects = [];
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) {
                    continue;
                }
                if (typeof obj[i] == 'object') {
                    objects = objects.concat(GlobalErrorService.prototype.getObjects(obj[i], key, val));
                } else if (i == key && obj[key] == val) {
                    objects.push(obj);
                }
            }
            return objects;
        };

        // Check if a response code is excepted, return it's action if it is (i.e global error service not to deal with it)
        GlobalErrorService.prototype.isExceptedError = function(StatusCode) {
            var isExcepted = null;
            isExcepted = GlobalErrorService.prototype.getObjects(ConfigService.errorExceptions, 'status', StatusCode);
            return (isExcepted !== undefined && isExcepted != null && isExcepted.length) ? isExcepted : false;
        };


        return GlobalErrorService;
    }])

    .config(['$provide', '$httpProvider', '$compileProvider', function($provide, $httpProvider, $compileProvider) {

        $httpProvider.responseInterceptors.push(function($timeout, $q, ConfigService, GlobalErrorService) {

            return function(promise) {

                return promise.then(function(successResponse) {
                    // Do nothing, this is the success response so hopefully we won't need to check for errors here
                    return successResponse;

                }, function(errorResponse) {
                    // Check each of the places the error could be, pass it to the messageToDisplay
                    var theError = '';

                    if (!!errorResponse.data.Result) {
                        theError = errorResponse.data.Result + ' ';
                    }

                    if (!!errorResponse.data.Reason) {
                        theError += errorResponse.data.Reason;
                    } else {
                        theError += 'Apologies, something appears to have gone wrong.\n';
                    }

                    var isExcepted = GlobalErrorService.prototype.isExceptedError(errorResponse.status);
                    if (!isExcepted) {
                        GlobalErrorService.prototype.messageToDisplay(errorResponse.status, theError, 'alert alert-danger');
                    }

                    return $q.reject(errorResponse);
                });
            };
        });

    }]);
}());