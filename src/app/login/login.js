(function() {

    'use strict';

    angular.module( 'helloJC.loginApp', [
        'ui.router'
        ])

    .config(function config( $stateProvider ) {
        $stateProvider
            .state('login', {
                url: '/login',
                views: {
                    'main': {
                        templateUrl: 'login/login.tpl.html',
                        controller: 'LoginCtrl'
                    }
                },
                data: {
                    authReq: false,
                    pageTitle: 'Login'
                }
        });
    })

    .controller( 'LoginCtrl', function LoginCtrl( FormService, ParseService, $scope, $state ) {
        /**
         * Provide JSON data / set defaults for each field on the form
         */
        var buildForm = function() {
            $scope.formFields = [
                {
                    key: 'username',
                    type: 'text',
                    label: 'Username',
                    placeholder: '',
                    required: true
                },
                {
                    key: 'password',
                    type: 'password',
                    label: 'Password',
                    placeholder: '',
                    required: true
                },
            ];
        };


        /**
         * Set options / assign methods used by the form
         */
        var initialiseForm = function() {
            $scope.showValidationSummary = false;
            $scope.formData = {};
            $scope.formOptions = {};

            /**
             * Submit the form data
             * Transition to appropriate state if successful / show validation summary if error
             * @return {[type]}
             */
            $scope.onSubmit = function() {
                ParseService.prototype.login($scope.formData,
                    function(user) {
                        $state.transitionTo('dave');
                    },
                    function(user, error) {
                        // error
                        // Handle any API errors that we want to show on the validation summary
                        // Note, we only really expect one error from the API
                        FormService.prototype.handleApiError(error, $scope.frmLogin);
                        FormService.prototype.setFormDirtyShowValidation($scope.frmLogin, $scope);
                    }
                );
            };
        };

        /**
         * Initialise app
         */
        var init = function() {
            initialiseForm();
            buildForm();
        };

        init();
    });
})();