(function () {

    'use strict';

    // Parse service

    angular
        .module('util.ParseServices', ['util.ConfigServices', 'util.InstagramServices'])
        .factory('ParseService', ['ConfigService', 'growl', 'InstagramService', '$q', function(ConfigService, growl, InstagramService, $q ) {

            var ParseService = function(){};

            /**
             * Get the data back from Parse
             * Push each object into the timeline date array
             * @return {[type]} [description]
             */
            ParseService.prototype.getTimelineItemData = function () {
                var deferred = $q.defer();

                // Add an inner query to negate Twitter timeline items from the
                // returned results.
                // Twitter oembed is failing as it needs https, which this
                // version of timelinejs doesn't seem to use
                // https://parse.com/docs/js/guide#queries-relational-queries
                var innerQuery = new Parse.Query('TimelineItemCategory');
                innerQuery.equalTo('category','Twitter');

                var TimelineItem = Parse.Object.extend('TimelineItem');
                var query = new Parse.Query(TimelineItem);
                query.doesNotMatchQuery('timelineItemCategory', innerQuery);

                query.find({
                    success: function(data) {
                        // The QUERY was successful
                        for (var i = 0; i < data.length; i++) {
                            var object = data[i];

                            // Add the title item to the top level of the timeline object
                            // This helps ensure we get a 'Return to Title' link
                            // next to the zoom controls
                            if (object.get('titleItem')) {
                                var titleItemObj = {
                                    headline: object.get('item').headline,
                                    text: object.get('item').text,
                                    asset: {
                                        media: object.get('item').asset.media,
                                        thumbnail: object.get('item').asset.thumbnail
                                    },
                                    endDate: object.get('item').endDate,
                                    startDate: object.get('item').startDate
                                };

                                angular.extend(ConfigService.timelineSource.timeline, titleItemObj);

                            } else {
                                ConfigService.timelineSource.timeline.date.push(object.get('item'));
                            }
                        }
                        deferred.resolve();
                    },
                    error: function(error) {
                        // The QUERY failed
                        deferred.reject();
                    }
                });

                return deferred.promise;
            };


            /**
             * Build a default Parse TimelineItem object
             * @return {object} Object with Parse TimelineItem structure
             */
            ParseService.prototype.getDefaultTimelineItemObj = function () {
                var defaultTimelineItem = {
                    item: {
                       asset:{
                          caption: '',
                          credit: '',
                          media: '',
                          thumbnail: ''
                       },
                       endDate: null,
                       headline: '',
                       startDate: null,
                       tag: '',
                       text: ''
                    }
                };
                return defaultTimelineItem;
            };


            /**
             * Format the form data into the object structure required by Parse
             * TODO
             * Can this be refactored to iterate over the default properties of
             * the defaultTimelineItemObj and populate by finding that value in
             * the formData?
             * @param  {object} formData    - $scope object containing the form data
             * @return {object}             - Object structured as a Parse
             *                                TimelineItem item
             */
            ParseService.prototype.buildTimelineItem = function (formData) {
                var newTimelineItem = ParseService.prototype.getDefaultTimelineItemObj();
                newTimelineItem.item.asset.caption = formData.caption;
                newTimelineItem.item.asset.credit = formData.credit;
                newTimelineItem.item.asset.media = formData.media;
                newTimelineItem.item.asset.thumbnail = formData.thumbnail;
                newTimelineItem.item.endDate = moment(formData.endDate).format('YYYY,MM,DD');
                newTimelineItem.item.headline = formData.headline;
                newTimelineItem.item.startDate = moment(formData.startDate).format('YYYY,MM,DD');
                newTimelineItem.item.text = formData.text + ParseService.prototype.buildTagList(formData.tags);
                return newTimelineItem;
            };


            /**
             * build the manual tag list (that I append to the end of the Parse
             * TimelineItem text property.
             * This ISN'T the TimelineJS tags but a list I use to show what
             * tech might have been used on a project
             * @param  {array} tagsArray    -  Comma delimited array of tags
             * @return {string}             - HTML string of list items
             */
            ParseService.prototype.buildTagList = function (tagsArray) {
                var tagsList = '<ul class="list-tags clearfix">';
                var splitTags = tagsArray.split(',');

                for (var i = 0; i < splitTags.length; i++) {
                    tagsList += '<li>' + splitTags[i].trim() + '</li>';
                }
                tagsList += '</ul>';
                return tagsList;
            };


            /**
             * Submit a users details for login
             * Set http header based on returned session token
             *
             * @param  {[type]}   formData [description]
             * @param  {Function} successCb [description]
             * @param  {[type]}   errorCb    [description]
             * @return {[type]}            [description]
             */
            ParseService.prototype.login = function (formData, successCb, errorCb) {
                Parse.User.logIn(formData.username, formData.password, {
                    success: function(user) {
                        // $http.defaults.headers.common['X-Parse-Session-Token'] = user._sessionToken;
                        successCb (user);
                    },
                    error: function(user, error) {
                        // The login failed. Check error to see why.
                        errorCb (user, error);
                    }
                });
            };


            return ParseService;
        }]);
}());