(function () {

    'use strict';

    // Config Services

    angular
        .module('util.ConfigServices', [])
        .factory('ConfigService', function() {

            var ConfigService = function(){};

            var envMode, protocol;
            envMode = document.location.hostname;
            protocol = document.location.protocol;

            /**
             * Set a global configuration block based on the detected environment
             * @param {[type]} environment [description]
             */
            ConfigService.prototype.setConfig = function(environment) {
                switch (envMode) {
                    case 'localhost':
                        // Development mode ConfigService object properties
                        angular.extend(ConfigService, {
                            currentEnvUrl: function() {
                                return protocol + '//' + envMode;
                            },
                            debug: true, // Use to apply debug style settings in code during testing
                            envMode: function() {
                                return envMode;
                            },
                            growlTimeout: 10000,
                            isDevMode: function() {
                                if (ConfigService.currentEnvUrl().indexOf('127.0.0.1') > -1  || ConfigService.currentEnvUrl().indexOf('localhost') > -1 || ConfigService.currentEnvUrl().indexOf('0.0.0.0') > -1) {
                                    return true;
                                } else {
                                    return false;
                                }
                            },
                            timelineItemCategories: [],
                            timelineSource: {
                                timeline: {
                                    headline: '',
                                    type: 'default',
                                    text: '',
                                    asset: {
                                        media: ''
                                    },
                                    date: []
                                }
                            },
                            timelineWidth: '100%',
                            timelineHeight: '94%',
                            timelineInitialZoom: 5
                        });
                        break;

                    // Production / default mode ConfigService object properties
                    default:
                        angular.extend(ConfigService, {
                            currentEnvUrl: function() {
                                return protocol + '//' + envMode;
                            },
                            debug: false, // Used to apply debug style settings in code during testing. Must always be false in production.
                            envMode: function() {
                                return envMode;
                            },
                            growlTimeout: 5000,
                            isDevMode: function() {
                                if (ConfigService.currentEnvUrl().indexOf('127.0.0.1') > -1  || ConfigService.currentEnvUrl().indexOf('localhost') > -1 || ConfigService.currentEnvUrl().indexOf('0.0.0.0') > -1) {
                                    return true;
                                } else {
                                    return false;
                                }
                            },
                            timelineItemCategories: [],
                            timelineSource: {
                                timeline:
                                {
                                    headline: '',
                                    type: 'default',
                                    text: '',
                                    asset: {
                                        media: ''
                                    },
                                    date: []
                                }
                            },
                            timelineWidth: '100%',
                            timelineHeight: '94%',
                            timelineInitialZoom: 5
                        });
                        break;
                }
            };

            ConfigService.prototype.setConfig();

            return ConfigService;
        });
}());